FROM ubuntu:latest
RUN apt update -y && apt upgrade -y && apt install -y python3-dev && apt install python3-pip && pip install django
WORKDIR /home/
RUN mkdir balasriharsha
WORKDIR /home/balasriharsha/
COPY . .
WORKDIR /home/balasriharsha/testdjango/testproject/
RUN python3 manage.py runserver